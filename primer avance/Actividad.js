// array = [1,2,3,4,5, "foo", "bar", true, 2.51]

// obj = {
//     'first_name': "Foo",
//     'last_name': 'Bar',
//     'age':23,
//     'status': true
// }
// console.log(typeof obj)
// console.log(obj["first_name"])
// console.log(obj.first_name)
// console.log(obj, array)

// for (let i = 0 ; i < 1000; i - 100)
// {
//     console.log(i)
// }

// for (var i of array){
//     console.log(i)
// }

// array.forEach(element => {
//     console.log(element)
// });

// i = 0;
// while(i < 10)
// {
//     console.log(i)
//     i++;
// }
// do{
//     console.log(i)
//     i++;
// }while(i<10)

// var x = 890
// if(x > 90){
//     console.log("Si es mayor")
// } else {
//     console.log("No es mayor")
// }

// var animal = 'Kitty'
// if(animal === 'Kitty'){
//     console.log("it is a pretty Kitty")
// } else {
//     console.log("Is not a pretty Kitty")
// }

// var cat = (animal === 'Kitty') ? console.log("It is a pretty Kitty") : console.log("Is not a pretty Kitty")

// switch (animal){
//     case "Kitty":
//         console.log("Case one")
//         break;
//     case "Pumpy":
//         console.log("Case two")
//         break;
//     default:
//         console.log("Case one")
//         break;
// }


// function prism(l){
//     return function(w){
//         return function(h){
//             return l * w * h
//         }
//     }
// }

// console.log(prism(89)(12)(9))

// const foo = (function(){
//     console.log("I am the IIFE");
// }());

// var message = "Hello word"
// const foo = (function(msg){
//     console.log("I am the IIFE" + msg);
//     return msg
// }(message));



// var namedSum = function sum(a,b){
//     return a = b;
// }

// var anonSum = function(a,b){
//     return a + b;
// }

// namedSum(1,3);
// anonSum(1,3);

// var say = function(times){
//     if(times > 0){
//         console.log("Hello")
//         say(times - 1)
//     }
// }

// var saysay = say
// say = "Dops!"
// saysay(100)

// function personSay(person, ...msg){
//     msg.forEach(arg => {
//         console.log(person + " say: " + arg)
//     })
// }
// personSay("Foo","Hello","JS","REACT","NATIVE","PWA")

//  fetch("")
//  .then()
